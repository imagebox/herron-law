// Utils
var fs      = require('fs');
var log     = require('fancy-log');
var chalk   = require('chalk');

// Gulps
var gulp    = require('gulp');
var watch   = require('gulp-watch');
var cat     = require('gulp-cat');
var concat  = require('gulp-concat');
var plumber = require('gulp-plumber');
var inject  = require('gulp-inject');
var rename  = require('gulp-rename');
var clone   = require('gulp-clone');

// JS
var uglify  = require('gulp-uglify');

// SASS
var sass      = require('gulp-sass');
var postcss   = require('gulp-postcss');
var sassGlob  = require('gulp-sass-glob');

// CSS
var cssnano         = require('cssnano');
var autoprefixer    = require('autoprefixer');
var mqpacker        = require('css-mqpacker');
var discardComments = require('postcss-discard-comments');

// SVG
var svgstore = require('gulp-svgstore');
var svgmin   = require('gulp-svgmin');

// BrowserSync
var browserSync = require('browser-sync').create();



/**
 * Serve
 */

gulp.task('browser-sync', function task_browser_sync() {

  /**
   * BrowserSync
   */

  browserSync.init({
    server: {
      baseDir: './site/'
    }
  });
});



/**
 * SASS
 * ----
 */

gulp.task('sass', function task_sass() {

  var processors = [
    autoprefixer({
      browsers: ['last 3 versions']
    }),
    mqpacker({
      sort: true
    }),
    discardComments()
  ];

  return gulp.src('./site/assets/sass/*.scss')
    .pipe(plumber(function (error) {
      log.error( error.message );
      this.emit('end');
    }))
    .pipe(sassGlob())
    .pipe(sass({
      includePaths: [
        './node_modules/breakpoint-sass/stylesheets'
      ],
      errLogToConsole: true,
      outputStyle: 'nested',
      precision: 12
    }))
    .pipe(postcss(processors))
    .pipe(gulp.dest('./site/assets/css'));
});


/**
 * CSS
 * ---
 * Creates min file while preserving the original
 */

gulp.task('css:minify', function task_css_minify() {
  
  var processors = [
    cssnano({
      discardComments: {
        removeAll: true
      }
    })
  ];

  return gulp.src([
    './site/assets/css/*.css',
    '!./site/assets/css/*.min.css'
  ])
    .pipe(clone())
    .pipe(rename(function (path) {
      path.basename += '.min';
      path.extname = '.css';
    }))
    .pipe(postcss(processors))
    .pipe(gulp.dest('./site/assets/css/'))
    .pipe(browserSync.stream());
});



/**
 * SVG
 * ---
 * [https://github.com/w0rm/gulp-svgstore]
 * [https://www.npmjs.com/package/gulp-svgmin]
 *
 * Combines and Inlines SVGs below the opening body tag
 */

gulp.task('svg', function task_svg() {
  var svgs = gulp.src('./site/assets/svg/**/*.svg')
    .pipe( svgmin( function (file) {
      return {
        plugins: [{
          removeViewBox: false
        }, {
          cleanupIDs: {
            minify: true
          }
        }]
      };
    }))
    .pipe( svgstore({ inlineSvg: true }));

  function fileContents(filePath, file) {
    return file.contents.toString();
  }

  return gulp.src([
    './site/index.html',
  ])
    .pipe(inject(svgs, { transform: fileContents }))
    .pipe(gulp.dest('./site/'));
});



/**
 * JS
 * --
 */

gulp.task('js', function task_js() {
  return gulp.src([
    './site/assets/js/libs/*.js',
    './site/assets/js/plugins/*.js',
    './site/assets/js/*.js'
  ])
    .pipe(plumber())
    .pipe(concat('site.js'))
    .pipe(gulp.dest('./site/assets/js/build'));
});

gulp.task('js:minify', function task_js_minify() {
  return gulp.src([
    './site/assets/js/build/*.js',
    '!./site/assets/js/build/*.min.js',
  ])
    .pipe(clone())
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.basename += '.min';
      path.extname = '.js';
    }))
    .pipe(gulp.dest( './site/assets/js/build/' ))
    .pipe(browserSync.stream());
});


/**
 * Gulp
 * ----
 */

// Watch
gulp.task( 'watch', function task_watch() {

  // Watch js files
  watch([
    './site/assets/js/**/*.js',
    '!./site/assets/js/build/**/*.js',
  ]).on('change', gulp.series( 'js', 'js:minify' ));

  // Watch scss files
  watch([
    './site/assets/sass/**/*.scss'
  ]).on('change', gulp.series( 'sass', 'css:minify' ));

  watch([
    './site/assets/svg/**/*.svg'
  ]).on('change', gulp.series( 'svg' ));
});

// Default Task
gulp.task('default', gulp.parallel( 'browser-sync', 'watch' ));
